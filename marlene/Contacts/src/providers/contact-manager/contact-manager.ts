import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Contacts, Contact, ContactField, ContactName, IContactProperties} from '@ionic-native/contacts';

/*
  Generated class for the ContactManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactManagerProvider {
//IContactProperties pour la completion
  currentContact:Contact=new Contact();
  //Data Transfert Object
  contactDto={
    id:0,
    display:'',
    firstName:'',
    lastName:'',
    email:'',
    phone:''
  };

  constructor(private contactsProvider: Contacts) {
    console.log('Hello ContactManagerProvider Provider');
  }
  getContactList():Promise<Contact[]>{
    return this.contactsProvider.find(["*"]);
  }
  addContact(contactDto):Promise<any>{
    let telContact = this.contactsProvider.create();
    telContact.name = new ContactName(null, contactDto.firstName, contactDto.lastName);
    telContact.emails = [new ContactField("email", contactDto.email)];
    telContact.phoneNumbers = [new ContactField("phone", contactDto.phone)];
    return telContact.save();
    
  }
  updateContact(contact){
    return this.contactsProvider.find(["*"],{
      filter:this.contactDto.id.toString(),
      multiple:false,
      desiredFields:["id"]
    }).then(data=>{
     //application des modifications
      data[0].name = new ContactName(null,"","");
      data[0].save();


    });
  }
  deleteContact(callback){
    return this.contactsProvider.find(["*"],{
      filter:this.contactDto.toString(),
      multiple:false,
      desiredFields:["id"]
    }).then(data=>{
     data[0].remove();
     data[0].save();
     if(callback)callback(data[0]);

    });
  }
  transformToDto(Contact){
    this.contactDto.id=Contact.id;
    this.contactDto.display=Contact.name.formatted;
    this.contactDto.firstName=Contact.name.familyName;
    this.contactDto.lastName=Contact.name.givenName;
    this.contactDto.email=Contact.emails==null?"No Email":Contact.emails[0].value;
    this.contactDto.phone=Contact.phoneNumbers==null?"No Phone Number":Contact.phoneNumbers[0].value;
  }
  transformFromDto(Dto){

  }

}
