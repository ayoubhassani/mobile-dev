import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, MenuController} from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactManagerProvider } from '../providers/contact-manager/contact-manager';
import {Contacts} from "@ionic-native/contacts";
import {FormsModule} from "@angular/forms";
import {UpdateFormPage} from "../pages/update-form/update-form";
//import {DeviceOrientation} from "@ionic-native/device-orientation";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Toast} from "@ionic-native/toast";
import {Calendar} from "@ionic-native/calendar";
import {PhotoLibrary} from "@ionic-native/photo-library";
import {PhotoPage} from "../pages/photo/photo";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    UpdateFormPage,
    PhotoPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    UpdateFormPage,
    PhotoPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactManagerProvider,
    MenuController,
    Contacts,
    //DeviceOrientation,
    ScreenOrientation,
    Toast,
    Calendar,
    PhotoLibrary
  ]
})
export class AppModule {}
