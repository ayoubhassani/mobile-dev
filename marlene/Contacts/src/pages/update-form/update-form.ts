import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ContactManagerProvider} from "../../providers/contact-manager/contact-manager";
import {ContactField, ContactName} from "@ionic-native/contacts";

/**
 * Generated class for the UpdateFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-form',
  templateUrl: 'update-form.html',
})
export class UpdateFormPage {
  newContact={
    id:0,
    display:'',
    firstName:'',
    lastName:'',
    email:'',
    phone:''
  };


  contact;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public InContact: ContactManagerProvider) {
    this.contact = this.navParams.get('contact');
    this.InContact.transformToDto(this.InContact.currentContact);
    this.newContact=this.InContact.contactDto;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateFormPage');
  }
  updateContact(event){
    this.InContact.currentContact.name = new ContactName(null,this.newContact.lastName, this.newContact.firstName);
    //this.InContact.currentContact.name.givenName = this.newContact.firstName;
    this.InContact.currentContact.emails = [new ContactField("email", this.newContact.email)];
    this.InContact.currentContact.phoneNumbers = [new ContactField("phone", this.newContact.phone)];
    // this.InContact.currentContact.save().then((saved)=>{
    //   console.log(saved);
    // })


  }
}
