import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ContactManagerProvider} from "../../providers/contact-manager/contact-manager";
//import {DeviceOrientation, DeviceOrientationCompassHeading} from "@ionic-native/device-orientation";
import {ScreenOrientation} from "@ionic-native/screen-orientation";




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contactsArray = [];



  constructor(public navCtrl: NavController,
              public InContact: ContactManagerProvider,
              //private deviceOrientation: DeviceOrientation,
              private screenOrientation: ScreenOrientation) {


    console.log(this.screenOrientation.type);

    // this.deviceOrientation.getCurrentHeading().then(
    //   (data: DeviceOrientationCompassHeading) => console.log(data),
    //   (error: any) => console.log(error)
    // );

  }


  ionViewDidLoad() {
    console.log("view home loaded trying to get contact list");
    this.InContact.getContactList().then(data => {
      console.log(data);
      this.contactsArray = data;
    })
  }

  contactSelected(contact) {
    this.InContact.currentContact=contact;
    this.InContact.transformToDto(contact)
  }

}
