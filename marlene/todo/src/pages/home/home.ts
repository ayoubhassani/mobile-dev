import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { TodoPage } from '../todo/todo';
import { AboutPage } from '../about/about';
import { ContactPage} from '../contact/contact'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  pageHome:any=TodoPage;
  contact:any=ContactPage;
  about:any=AboutPage;
  constructor(public navCtrl: NavController) {
  }



}
