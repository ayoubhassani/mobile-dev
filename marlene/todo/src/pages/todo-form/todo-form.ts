import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {TodoProvider} from "../../providers/todo/todoProvider";

/**
 * Generated class for the TodoFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todo-form',
  templateUrl: 'todo-form.html',
})
export class TodoFormPage {
  todo:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public srv:TodoProvider) {
    //console.log(this.navParams.get('todo'));
    this.todo=this.navParams.get('todo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoFormPage');
  }
  back(){
    this.navCtrl.pop();
  }
  cancelForm(){
    this.viewCtrl.dismiss();
  }
  validateForm(){
    if(this.todo.id==undefined){
      this.srv.addTodo(this.todo).subscribe(addedtodo=>{
        this.srv.tableauDesTodos.unshift(addedtodo);
        //this.srv.tableauDesTodos.push(addedtodo);
        this.viewCtrl.dismiss();
      })
    }
else {
      this.viewCtrl.dismiss()
    }

  }

}
