import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {TodoDetailsPage} from "../todo-details/todo-details";
import {TodoProvider} from "../../providers/todo/todoProvider";
import {TodoFormPage} from "../todo-form/todo-form";


@IonicPage()
@Component({
  selector: 'page-todo',
  templateUrl: 'todo.html',
})
export class TodoPage {
  selectedTodo:any;
  pageDetails:any= TodoDetailsPage;
  formPage:any= TodoFormPage;
  TodoList:any[]=[
    {id:1, titre:'Titre 1', body:'1 Vbi curarum abiectis ponderibus aliis tamquam nodum et codicem difficillimum Caesarem convellere nisu valido cogitabat, eique deliberanti cum proximis clandestinis conloquiis et nocturnis qua vi, quibusve commentis id fieret, antequam effundendis rebus pertinacius incumberet confidentia, acciri mollioribus scriptis per simulationem tractatus publici nimis urgentis eundem placuerat Gallum, ut auxilio destitutus sine ullo interiret obstaculo.\n' +
    '\n' +
    'Et eodem impetu Domitianum praecipitem per scalas itidem funibus constrinxerunt, eosque coniunctos per ampla spatia civitatis acri raptavere discursu. iamque artuum et membrorum divulsa conpage superscandentes corpora mortuorum ad ultimam truncata deformitatem velut exsaturati mox abiecerunt in flumen.\n' +
    '\n' +
    'Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere. in quibus hoc primum est in quo admirer, cur in gravissimis rebus non delectet eos sermo patrius, cum idem fabellas Latinas ad verbum e Graecis expressas non inviti legant. quis enim tam inimicus paene nomini Romano est, qui Ennii Medeam aut Antiopam Pacuvii spernat aut reiciat, quod se isdem Euripidis fabulis delectari dicat, Latinas litteras oderit?\n' +
    '\n' +
    'Nunc vero inanes flatus quorundam vile esse quicquid extra urbis pomerium nascitur aestimant praeter orbos et caelibes, nec credi potest qua obsequiorum diversitate coluntur homines sine liberis Romae.\n' +
    '\n' +
    'Ergo ego senator inimicus, si ita vultis, homini, amicus esse, sicut semper fui, rei publicae debeo. Quid? si ipsas inimicitias, depono rei publicae causa, quis me tandem iure reprehendet, praesertim cum ego omnium meorum consiliorum atque factorum exempla semper ex summorum hominum consiliis atque factis mihi censuerim petenda.'},
    {id:2, titre:'Titre 2', body:' 2 Vbi curarum abiectis ponderibus aliis tamquam nodum et codicem difficillimum Caesarem convellere nisu valido cogitabat, eique deliberanti cum proximis clandestinis conloquiis et nocturnis qua vi, quibusve commentis id fieret, antequam effundendis rebus pertinacius incumberet confidentia, acciri mollioribus scriptis per simulationem tractatus publici nimis urgentis eundem placuerat Gallum, ut auxilio destitutus sine ullo interiret obstaculo.\n' +
    '\n' +
    'Et eodem impetu Domitianum praecipitem per scalas itidem funibus constrinxerunt, eosque coniunctos per ampla spatia civitatis acri raptavere discursu. iamque artuum et membrorum divulsa conpage superscandentes corpora mortuorum ad ultimam truncata deformitatem velut exsaturati mox abiecerunt in flumen.\n' +
    '\n' +
    'Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere. in quibus hoc primum est in quo admirer, cur in gravissimis rebus non delectet eos sermo patrius, cum idem fabellas Latinas ad verbum e Graecis expressas non inviti legant. quis enim tam inimicus paene nomini Romano est, qui Ennii Medeam aut Antiopam Pacuvii spernat aut reiciat, quod se isdem Euripidis fabulis delectari dicat, Latinas litteras oderit?\n' +
    '\n' +
    'Nunc vero inanes flatus quorundam vile esse quicquid extra urbis pomerium nascitur aestimant praeter orbos et caelibes, nec credi potest qua obsequiorum diversitate coluntur homines sine liberis Romae.\n' +
    '\n' +
    'Ergo ego senator inimicus, si ita vultis, homini, amicus esse, sicut semper fui, rei publicae debeo. Quid? si ipsas inimicitias, depono rei publicae causa, quis me tandem iure reprehendet, praesertim cum ego omnium meorum consiliorum atque factorum exempla semper ex summorum hominum consiliis atque factis mihi censuerim petenda.'}
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public srv:TodoProvider, public actionSheetCtrl: ActionSheetController, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoPage');
    this.srv.getAllTodos().subscribe(arrayTodos=>{
      this.srv.tableauDesTodos=arrayTodos;
    })
  }
  itemSelected(todo){
    this.navCtrl.push(this.pageDetails, todo);
    //this.navCtrl.push(this.formPage, {todo:todo});

  }
showActionSheet(event){
    this.selectedTodo=event;
  //console.log(event);
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Action on todo: '+ event.titre,

   buttons: [
  {
   text: 'Modifier',
   role: 'update',
   handler: () => {
   //console.log('Modifier clicked');
   this.showTodoForm(this.selectedTodo);
   }
  },{
    text: 'Supprimer',
    role: 'delete',
    handler: () => {

      this.srv.deleteTodos(this.selectedTodo).subscribe(data=>{
        this.srv.tableauDesTodos=this.srv.tableauDesTodos.filter((todo)=>{
          return todo.id != this.selectedTodo.id;
        })
      });
     // console.log('Supprimer clicked');
    }
   }
   ]
   });
   actionSheet.present();
}
  showTodoForm(todo){
    let modal = this.modalCtrl.create(TodoFormPage,{todo:todo}, {showBackdrop:true});
    modal.present();
  }

}
