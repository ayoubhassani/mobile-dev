import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult} from "@ionic-native/native-geocoder";
import {GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent} from "@ionic-native/google-maps";

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  pos:any={lat:1, lon:0};
  addr:any;
  map: GoogleMap;
  mapElement: HTMLElement;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public geolocation:Geolocation,
              private nativeGeocoder: NativeGeocoder,
              private googleMaps: GoogleMaps) {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("promise getCurrentPosition",resp);
    this.pos.lat=resp.coords.latitude;
    this.pos.lon=resp.coords.longitude;

      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude).then((result:NativeGeocoderReverseResult)=>{
        this.addr=result;
        console.log(JSON.stringify(this.addr));

      }).catch((error: any) => console.log(error));

      //console.log('lat: ' + this.pos.lat + ', lon: ' + this.pos.lon);
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
    //this.loadMap();
  }

  loadMap() {
    this.mapElement = document.getElementById('map');

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.pos.lat,
          lng: this.pos.lon
        },
        zoom: 18,
        tilt: 30
      }
    };
    this.map = this.googleMaps.create(this.mapElement, mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');

        // Now you can use all methods safely.
        this.map.addMarker({
          title: 'Ionic',
          icon: 'blue',
          animation: 'DROP',
          position: {
            lat: this.pos.lat,
            lng: this.pos.lon
          }
        })
          .then(marker => {
            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                alert('clicked');
              });
          });

      });
  }



}

