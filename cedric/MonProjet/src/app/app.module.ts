import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {AboutPage} from "../pages/about/about";
import {ContactPage} from "../pages/contact/contact";
import {TodosPage} from "../pages/todos/todos";
import {TododetailsPage} from "../pages/tododetails/tododetails";
import {TodoProvider} from '../providers/todo/todoProvider';
import {HttpModule} from "@angular/http";
import {TodoformPage} from "../pages/todoform/todoform";
import {FormsModule} from "@angular/forms";
import {Geolocation} from "@ionic-native/geolocation";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import {GoogleMaps} from "@ionic-native/google-maps";
import {ContactsPage} from "../pages/contacts/contacts";
import {Contacts} from "@ionic-native/contacts";
import {ListcontactsPage} from "../pages/listcontacts/listcontacts";
import {PlusuncontactPage} from "../pages/plusuncontact/plusuncontact";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    ContactPage,
    TodosPage,
    TododetailsPage,
    TodoformPage,
    ContactsPage,
    ListcontactsPage,
    PlusuncontactPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    ContactPage,
    TodosPage,
    TododetailsPage,
    TodoformPage,
    ContactsPage,
    ListcontactsPage,
    PlusuncontactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    GoogleMaps,
    Contacts,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TodoProvider
  ]
})
export class AppModule {}
