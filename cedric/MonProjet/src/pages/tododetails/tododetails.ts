import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams,
        ToastController} from 'ionic-angular';
import {TodoProvider} from "../../providers/todo/todoProvider";
import {TodoformPage} from "../todoform/todoform";
import {HomePage} from "../home/home";
import {TodosPage} from "../todos/todos";

@IonicPage()
@Component({
  selector: 'page-tododetails',
  templateUrl: 'tododetails.html',
})
export class TododetailsPage {

  todo: any;
  todoVide: any = {};
  ph1:string = "Todo Deleted successfully";
  ph2:string = "Todo Update successfully";
  ph3:string = "Todo Added successfully";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public srv: TodoProvider,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController) {
    console.log(this.navParams);
    this.todo = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('TododetailsPage');
  }
  ShowTodoForm(todo) {
    let modal = this.modalCtrl.create(TodoformPage, {todo: todo}, {showBackdrop: true});
    modal.present();
  }
  presentActionSheet(todo) {
    console.log(todo);
    //let innerThis=this;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your Todos',
      buttons: [
        {
          text: 'Ultimate Destruction !!!! PPPRPRPRPRRRRRRRR',
          role: 'destructive',
          handler: () => {
            this.srv.deleteTodo(todo).subscribe(data => {
              this.srv.tabDesTodos = this.srv.tabDesTodos.filter(todo => {
                return this.todo.id !== todo.id;
              });
              //innerThis.navCtrl.push(TodosPage);
            });
            this.presentToast(this.ph1);
            this.navCtrl.pop();
          }
        }, {
          text: 'Modifier',
          handler: () => {
            this.ShowTodoForm(todo);
            console.log('Archive clicked');
          }
        },
        {
          text: 'Ajouter',
          role: 'add',
          handler: () => {
            this.ShowTodoForm(this.todoVide);
            console.log('Add clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  preventPage($event) {
    console.log($event);
    if($event.deltaX > 0) this.navCtrl.pop();
  }
  presentToast(nb) {
    let toast = this.toastCtrl.create({
      message: nb,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}

