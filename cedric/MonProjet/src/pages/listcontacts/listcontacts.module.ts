import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListcontactsPage } from './listcontacts';

@NgModule({
  declarations: [
    ListcontactsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListcontactsPage),
  ],
})
export class ListcontactsPageModule {}
