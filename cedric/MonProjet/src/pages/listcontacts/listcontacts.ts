import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Contact, ContactField, ContactName, Contacts} from "@ionic-native/contacts";
import {PlusuncontactPage} from "../plusuncontact/plusuncontact";

@IonicPage()
@Component({
  selector: 'page-listcontacts',
  templateUrl: 'listcontacts.html',
})
export class ListcontactsPage {
  PageAddCtc: any = PlusuncontactPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public contacts: Contacts,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListcontactsPage');
  }

  itemSelected() {
    console.log();
    this.navCtrl.push(this.PageAddCtc);
  }
  ShowTodoForm(todo) {
    let modal = this.modalCtrl.create(PlusuncontactPage, {aze: aze}, {showBackdrop: true});
    modal.onDidDismiss(data => {

      todo = {
        id: innerTodo.id,
        titre: innerTodo.titre,
        body: innerTodo.body,
      };
      console.log(todo)
    });
    modal.present();
  }


  ajoutContact(firstname, lastname, phone, email){
    let contact: Contact = this.contacts.create();

    contact.name = new ContactName(null, lastname, firstname);
    contact.phoneNumbers = [new ContactField('mobile', phone)];
    contact.emails = [new ContactField('email', email)];
    contact.save().then( () => console.log('Contact saved!', contact),
      (error: any) => console.error('Error saving contact.', error)
    );
  }

}
