import { Component } from '@angular/core';
import {AlertController, NavController} from "ionic-angular";
import { TodosPage } from '../todos/todos';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import {ListcontactsPage} from "../listcontacts/listcontacts";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  todopage = TodosPage;
  aboutpage = AboutPage;
  contactpage = ContactPage;
  mescontacts = ListcontactsPage;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
  }

  showAlert(){
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }
}
