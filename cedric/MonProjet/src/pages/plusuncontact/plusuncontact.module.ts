import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlusuncontactPage } from './plusuncontact';

@NgModule({
  declarations: [
    PlusuncontactPage,
  ],
  imports: [
    IonicPageModule.forChild(PlusuncontactPage),
  ],
})
export class PlusuncontactPageModule {}
