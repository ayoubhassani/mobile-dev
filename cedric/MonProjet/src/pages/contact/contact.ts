import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {NativeGeocoder, NativeGeocoderReverseResult} from "@ionic-native/native-geocoder";
import {GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions,
        CameraPosition, MarkerOptions, Marker} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  position: any = {lat: 44.339817, lon: 1.205287};
  address: any;
  map: GoogleMap;
  mapElement: HTMLElement;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public geolocation: Geolocation,
              public nativeGeocoder: NativeGeocoder,
              public googleMaps: GoogleMaps) {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.position.lat = resp.coords.latitude;
      this.position.lon = resp.coords.longitude;
      this.nativeGeocoder.reverseGeocode(this.position.lat, this.position.lon)
        .then((result: NativeGeocoderReverseResult) => {
          this.address = result;
          console.log(JSON.stringify(this.address))
        })
        .catch((error: any) => console.log(error));
      console.log(this.position.lat, this.position.lon)
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  ionViewDidLoad() {
    this.loadMap();
    console.log('ContactPage');
  }
  loadMap() {
    this.mapElement = document.getElementById('map');

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = this.googleMaps.create(this.mapElement, mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');

        // Now you can use all methods safely.
        this.map.addMarker({
          title: 'Ionic',
          icon: 'blue',
          animation: 'DROP',
          position: {
            lat: 43.0741904,
            lng: -89.3809802
          }
        })
          .then(marker => {
            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                alert('clicked');
              });
          });

      });
  }


}
