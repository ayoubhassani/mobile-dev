import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoformPage } from './todoform';

@NgModule({
  declarations: [
    TodoformPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoformPage),
  ],
})
export class TodoformPageModule {}
