import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {TodoProvider} from "../../providers/todo/todoProvider";

@IonicPage()
@Component({
  selector: 'page-todoform',
  templateUrl: 'todoform.html',
})
export class TodoformPage {

  todo: any;
  ph1:string = "Todo Deleted successfully";
  ph2:string = "Todo Update successfully";
  ph3:string = "Todo Added successfully";
  inneTodo="";

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController,
              public srv: TodoProvider,
              public toastCtrl: ToastController) {
    this.todo = this.navParams.get('todo');
    this.inneTodo=this.todo;
    console.log(this.todo);
  }

  ionViewDidLoad() {
    console.log('TodoformPage');
  }
  cancelForm() {
    this.viewCtrl.dismiss();
  }
  validateForm() {
    console.log(this);
    if (this.todo.id === undefined) {
      this.srv.addTodos(this.todo).subscribe(addedTodo => {
        this.srv.tabDesTodos.push(addedTodo);
        this.viewCtrl.dismiss();
        this.presentToast(this.ph3)
      });
    } else {
      this.srv.updateTodo(this.todo).subscribe(modifidedTodo => {
        this.viewCtrl.dismiss();
        this.presentToast(this.ph2)
      })
    }
  }
  presentToast(nb) {
    let toast = this.toastCtrl.create({
      message: nb,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}



