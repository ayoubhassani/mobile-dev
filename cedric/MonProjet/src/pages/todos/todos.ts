import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams,
        ToastController} from 'ionic-angular';
import {TododetailsPage} from "../tododetails/tododetails";
import {TodoProvider} from "../../providers/todo/todoProvider";
import {TodoformPage} from "../todoform/todoform";
import {noUndefined} from "@angular/compiler/src/util";

@IonicPage()
@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html',
})
export class TodosPage {

  searchQuery: string = '';
  items: string[];
  PageDetails: any = TododetailsPage;
  selected: any;
  todoVide: any = {};
  ph1:string = "Todo Deleted successfully";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public srv: TodoProvider,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController) {
  }

  initializeItems() {
    this.items = [
      'Amsterdam',
      'Bogota'
    ];
  }
  ionViewDidLoad() {
    console.log('TodosPage');
    this.srv.getAllTodos().subscribe(arrayTodos => {
      this.srv.tabDesTodos = arrayTodos;
    })
  }
  itemSelected(todo, $event) {
    console.log(todo);
    if($event.deltaX < 0) this.navCtrl.push(this.PageDetails, todo);
    if($event.deltaX == undefined) this.navCtrl.push(this.PageDetails, todo);
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  ShowTodoForm(todo) {
    let innerTodo = {
     id: todo.id,
     titre: todo.titre,
     body: todo.body,
    };
    let modal = this.modalCtrl.create(TodoformPage, {todo: todo}, {showBackdrop: true});
    modal.onDidDismiss(data => {

      todo = {
        id: innerTodo.id,
        titre: innerTodo.titre,
        body: innerTodo.body,
      };
      console.log(todo)
    });
    modal.present();
  }
  presentToast(nb) {
    let toast = this.toastCtrl.create({
      message: nb,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  actionSheet(todo){
    //(todo !== undefined)? this.presentActionSheet(todo): this.addActionSheet();
    if(todo != undefined){
      this.presentActionSheet(todo);
      return
    }else {
      this.addActionSheet();
      return
    }
  }
  addActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Add Todo',
      buttons: [
        {
          text: 'Ajouter',
          role: 'add',
          handler: () => {
            this.ShowTodoForm(this.todoVide);
            console.log('Add clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  presentActionSheet(todo) {
    console.log(todo);
    this.selected = todo;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your Todo',
      buttons: [
        {
          text: 'Ultimate Destruction',
          role: 'destructive',
          handler: () => {
            this.srv.deleteTodo(todo).subscribe(data => {
              this.srv.tabDesTodos = this.srv.tabDesTodos.filter(todo => {
                return this.selected.id !== todo.id;
              });
            });
            this.presentToast(this.ph1);
          }
        }, {
          text: 'Modifier',
          handler: () => {
            this.ShowTodoForm(todo);
            console.log('Archive clicked');
          }
        }, {
          text: 'Ajouter',
          role: 'add',
          handler: () => {
            this.ShowTodoForm(this.todoVide);
            console.log('Add clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
