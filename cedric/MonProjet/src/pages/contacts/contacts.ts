import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Contact, ContactField, ContactName, Contacts} from "@ionic-native/contacts";

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {

  contactsList:any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public contacts: Contacts,
              public toastCtrl: ToastController){
  }

  ionViewDidLoad() {
    console.log('MesContactsPage');
    this.ajoutContact("Cédric","Salaun", "00065650650", "azer@azer/com");
  }



  ajoutContact(firstname, lastname, phone, email){
    let contact: Contact = this.contacts.create();

    contact.name = new ContactName(null, lastname, firstname);
    contact.phoneNumbers = [new ContactField('mobile', phone)];
    contact.emails = [new ContactField('email', email)];
    contact.save().then( () => console.log('Contact saved!', contact),
      (error: any) => console.error('Error saving contact.', error)
    );
  }

}
