import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

@Injectable()
export class TodoProvider {
  url: string = "http://192.168.1.47:8090/todo";
  tabDesTodos:any[];

  constructor(public http: Http) {
    console.log('Hello TodoProvider Provider');
  }

  getAllTodos(): Observable<Todo[]> {
    return this.http.get(this.url).map(res => res.json());
  }
  addTodos(param): Observable<Todo[]> {
    return this.http.put(this.url, param).map(res => res.json());
  }
  findTodos(param): Observable<Todo[]> {
    return this.http.get(this.url +'/search/', param).map(res => res.json());
  }
  deleteTodo(param): Observable<Todo[]> {
    return this.http.delete(this.url ,{body:param}).map(res => res.json());
  }
  updateTodo(param): Observable<Todo[]> {
    return this.http.post(this.url, param).map(res => res.json());
  }

}

interface Todo {
  id: number;
  titre: string;
  body: string;
}
