import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Contacts, Contact, ContactField, ContactName, IContactProperties} from '@ionic-native/contacts';

/*
  Generated class for the ContactManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactManagerProvider {
  //IContactProperties for code completion
  curentContact:Contact= new Contact();
  //DATA Transfert Object
  contactDto = {
    id: 0,
    display: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
  };
  constructor(private contactsprovider: Contacts) {
    console.log('Hello ContactManagerProvider Provider');
  }

  getContactList(): Promise<Contact[]> {
    return this.contactsprovider.find(["*"]);
  }

  addContact(contactDto):Promise<any>{
     let telContact =   this.contactsprovider.create();
     telContact.name  = new ContactName(null,contactDto.firstName,contactDto.lastName);
    telContact.emails = [new ContactField("email",contactDto.email)];
    telContact.phoneNumbers = [new ContactField("phone",contactDto.phone)];
    return telContact.save();
  }

  updateContact(contact) {

    this.contactsprovider.find(["*"],{
      filter:this.curentContact.id.toString(),
      multiple:false,
      desiredFields:["*"]
    }).then(data=>{
      data[0].name = new ContactName(null,contact.firstName,contact.lastName);
      data[0].emails = [new ContactField("email",contact.email)];
      data[0].phoneNumbers = [new ContactField("phone",contact.phone)];
/*      return data[0].save().then((saved)=>{
        console.log(saved);
      });*/
    });
/*
    this.curentContact.name = new ContactName(null, contact.firstName, contact.lastName);
    //this.InContact.curentContact.name.givenName = this.newContact.lastName
    this.curentContact.emails = [new ContactField("email", contact.email)];
    this.curentContact.phoneNumbers = [new ContactField("phone", contact.phone)];
    return this.curentContact.save()*/
  }

  deleteContact(callback) {
    console.log('"id"='+this.contactDto.id+'')
    this.contactsprovider.find(["*"],{
      filter:this.contactDto.id.toString(),
      multiple:false,
      desiredFields:["id"]
    }).then(data=>{
      data[0].remove();
      data[0].save()
      if(callback)callback(data[0]);
    });
  }




  transToDto(Contact) {
    this.contactDto.id = Contact.id;
    this.contactDto.display = Contact.name.formatted;
    this.contactDto.firstName = Contact.name.familyName;
    this.contactDto.lastName = Contact.name.givenName;
    this.contactDto.email = Contact.emails == null ? "No Email" : Contact.emails[0].value;
    this.contactDto.phone = Contact.phoneNumbers == null ? "No Phone" : Contact.phoneNumbers[0].value;
  }

  transformDto(Dto) {

  }

}
