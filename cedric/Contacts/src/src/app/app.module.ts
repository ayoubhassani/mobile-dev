import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, MenuController} from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactManagerProvider } from '../providers/contact-manager/contact-manager';
import {Contacts} from "@ionic-native/contacts";
import {FormsModule} from "@angular/forms";
import {UpdateFormPage} from "../pages/update-form/update-form";
import {DeviceOrientation} from "@ionic-native/device-orientation";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    UpdateFormPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    UpdateFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Contacts,
    MenuController,
    DeviceOrientation,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactManagerProvider
  ]
})
export class AppModule {}
