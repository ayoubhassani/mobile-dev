import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ContactManagerProvider} from "../../providers/contact-manager/contact-manager";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-list-contact',
  templateUrl: 'list-contact.html',
})
export class ListContactPage {
  newContact = {
    id:0,
    firstName: "",
    givenName:"",
    phoneNumber:"",
    email:""
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private cntManager: ContactManagerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListContactPage');
  }
  validateNewcontact(e){
    e.preventDefault();
    /*this.cntManager.addContact(this.newContact).then(contactData=>{
      this.navCtrl.setRoot(HomePage);
      console.log('add' + contactData);
    });*/
  }

}
