import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Contacts, Contact, ContactField, ContactName, IContactProperties} from '@ionic-native/contacts';

/*
  Generated class for the ContactManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactManagerProvider {
//IContactProperties for code completion
  currentContact:Contact = new Contact();
  //Data Transfert Object
  contactDto={
    id:0,
    display:"",
    firstName:"",
    lastName:"",
    email:"",
    phone:""
  };

  constructor(private contactsProvider: Contacts) {
    console.log('Hello ContactManagerProvider Provider');
  }

  getContactList():Promise<Contact[]>{
    return this.contactsProvider.find(["*"]);
  }

  addContact(contactDto):Promise<any>{
    let telContact = this.contactsProvider.create();
      telContact.name = new ContactName(null, contactDto.firstName, contactDto.lastName);
      telContact.emails = [ new ContactField('email',contactDto.email)];
      telContact.phoneNombers = [new ContactField('phone',contactDto.phone)];
      return telContact.save();
  }

  updateContact(contact){
    console.log('"id"='+this.contactDto.id+'');
    this.contactsProvider.find(["*"],{
      filter:this.contactDto.id.toString(),
      multiple:false,
      desiredFields:["*"],
      hasPhoneNumber:true
    }).then(data=>{
      //application des modifications
      console.log(data);
      data[0].name = new ContactName(null,"","");
      data[0].save();
      //if(callback)callback(data[0]);
    });

  }

  deleteContact(callback){
    this.contactsProvider.find(["*"],{
      filter:this.contactDto.id.toString(),
      multiple:false,
      desiredFields:["*"],
      hasPhoneNumber:true
    }).then(data=>{
      console.log(data);
      data[0].remove();
      if(callback)callback(data[0]);
    });
  }

  transToDto(contact){
    this.contactDto.id = contact.id;
    this.contactDto.display = contact.name.formatted;
    this.contactDto.firstName = contact.name.familyName;
    this.contactDto.lastName = contact.name.givenName;
    this.contactDto.email = contact.emails==null?"No email":contact.emails[0].value;
    this.contactDto.phone = contact.phoneNumbers==null?"No Phone number":contact.phoneNumbers[0].value;
  }

  transformDto(Dto){

  }
}
