import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ContactManagerProvider} from "../../providers/contact-manager/contact-manager";
// import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contactArray=[];


  constructor(public navCtrl: NavController,
              public InContact:ContactManagerProvider){
              // private deviceOrientation: DeviceOrientation)

    // this.deviceOrientation.getCurrentHeading().then(
    //   (data: DeviceOrientationCompassHeading) => console.log(data),
    //   (error: any) => console.log(error)
    // );


  }

  ionViewDidLoad(){
    console.log("View Home Loaded Trying to get Contacts List");
    this.InContact.getContactList().then(data => {
      console.log(data);
      this.contactArray=data;
    })
  }

  contactSelected(contact){
    this.InContact.currentContact = contact;
    this.InContact.transToDto(contact)
  }

  getItems(contact){

  }




}
