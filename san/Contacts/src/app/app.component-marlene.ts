import { Component, ViewChild } from '@angular/core';
import {ModalController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {ContactManagerProvider} from "../providers/contact-manager/contact-manager";
import {UpdateFormPage} from "../pages/update-form/update-form";
//import {DeviceOrientation, DeviceOrientationCompassHeading} from "@ionic-native/device-orientation";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Toast} from "@ionic-native/toast";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  updateForm=UpdateFormPage;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;


  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private InContact: ContactManagerProvider,
              public modalCtrl: ModalController,
              //private deviceOrientation: DeviceOrientation,
              private screenOrientation: ScreenOrientation,
              private toast: Toast) {
    // var subscription = this.deviceOrientation.watchHeading().subscribe(
    //   (data: DeviceOrientationCompassHeading) => console.log(data)
    // );
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'All Contacts', component: HomePage },
      { title: 'New Contact', component: ListPage },
      { title: 'Todos', component: HomePage },
      { title: 'Todos2', component: HomePage }
    ];

  }

  showToast(){
    this.toast.show(`position vue : `+this.screenOrientation.type, '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
    // if(this.screenOrientation.type == 'landscape'){
    //     toast => {
    //       console.log(toast);
    //     }
    //   );
    // }else {
    //   this.toast.show(`I'm a toast en portrait`, '5000', 'center').subscribe(
    //     toast => {
    //       console.log('portrait', toast);
    //     }
    //   )
    //
    // }
  }
  initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.screenOrientation.onChange().subscribe(
        () => {
          console.log("Orientation Changed"+this.screenOrientation.type);
          this.showToast();
        }
      );


    });
  }
  openEdit(e){
    console.log('opening');
    this.modalCtrl.create(this.updateForm, {contact:{test:'le contact que je veux charger dans le formulaire'}}).present();
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


}
