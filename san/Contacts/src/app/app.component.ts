import { Component, ViewChild } from '@angular/core';
import {ModalController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {ContactManagerProvider} from "../providers/contact-manager/contact-manager";
import {UpdateFormPage} from "../pages/update-form/update-form";
// import {DeviceOrientation, DeviceOrientationCompassHeading} from "@ionic-native/device-orientation";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Toast} from "@ionic-native/toast";
import {PhotoPage} from "../pages/photo/photo";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  updateForm = UpdateFormPage;
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public InContact:ContactManagerProvider,
              public modalCtrl: ModalController,
              // private deviceOrientation: DeviceOrientation,
              private screenOrientation: ScreenOrientation,
              private toast: Toast) {
    console.log('constructor ', this.screenOrientation.type);
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'All contacts', component: HomePage },
      { title: 'New Contact', component: ListPage },
      { title: 'Photos', component: PhotoPage }
    ];

  }

  initializeApp() {
    console.log('initializeApp ', this.screenOrientation.type);
    this.platform.ready().then(() => {
      console.log('initializeApp Rea', this.screenOrientation.type);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // var subscription = this.deviceOrientation.watchHeading().subscribe(
      //   (data: DeviceOrientationCompassHeading) => console.log(data)
      // );

      this.screenOrientation.onChange().subscribe(
        () => {
          console.log("Orientation Changed" + this.screenOrientation.type);
          this.showToast();
        }
      );
    });
  }

  showToast(){
    console.log('dans showtoast');
    this.toast.show(`je suis en ` + this.screenOrientation.type, '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
  }
  openEdit(e){
    console.log('opening');
    this.modalCtrl.create(this.updateForm,{contact:{test:'uytgeyfg'}}).present();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
