import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {TodoProvider} from "../../providers/todo/todoProvider";

/**
 * Generated class for the TodoFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todo-form',
  templateUrl: 'todo-form.html',
})
export class TodoFormPage {

  todo:any=[];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public srv:TodoProvider) {
    this.todo = navParams.get('todo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoFormPage');
  }

  cancelForm(){
    this.viewCtrl.dismiss();
  }

  validateForm(){
    if(this.todo.id == undefined){
      // Ajout de todo
      this.srv.addTodo(this.todo).subscribe(addedTodo=>{
      //this.srv.tableauDesTodos.push(addedTodo);
        this.srv.tableauDesTodos.unshift(addedTodo);
        this.viewCtrl.dismiss();
      })
    } else {
      // modification de todo
      this.viewCtrl.dismiss();
    }
  }

  back(){
    this.navCtrl.pop();
  }
}
