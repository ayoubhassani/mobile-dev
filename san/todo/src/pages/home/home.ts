import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {TodoPage} from "../todo/todo";
import {AboutPage} from "../about/about";
import {ContactPage} from "../contact/contact";
import {MesContactsPage} from "../mes-contacts/mes-contacts";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pageHome:any = TodoPage;
  contact:any = ContactPage;
  about:any = AboutPage;
  mesContacts:any = MesContactsPage;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {

  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }
}

