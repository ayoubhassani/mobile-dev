import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

/**
 * Generated class for the MesContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mes-contacts',
  templateUrl: 'mes-contacts.html',
})
export class MesContactsPage {
contact:InContact = {
  id:0,
  name:"",
  email:"",
  phone:""
};
contactsList:InContact[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public contacts: Contacts) {

  }


  ionViewDidLoad() {
    this.contacts.find(["*"]).then(arrayContact=>{
      console.log(arrayContact)
    });
  }

  createContact(){
      let contact: Contact = this.contacts.create();
      contact.name = new ContactName(null,this.contact.name);
      contact.phoneNumbers = [new ContactField('mobile', this.contact.phone)];
      contact.emails = [new ContactField('email', this.contact.email)];
      contact.save().then(
        () => console.log('Contact saved!', contact),
        (error: any) => console.error('Error saving contact.', error)
      );
    }
}

interface InContact{
  id:number;
  name:string;
  phone:string;
  email:string;
}


