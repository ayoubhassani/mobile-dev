import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesContactsPage } from './mes-contacts';

@NgModule({
  declarations: [
    MesContactsPage,
  ],
  imports: [
    IonicPageModule.forChild(MesContactsPage),
  ],
})
export class MesContactsPageModule {}
