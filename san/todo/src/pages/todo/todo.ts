import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {TodoDetailsPage} from "../todo-details/todo-details";
import {TodoProvider} from "../../providers/todo/todoProvider";
import {TodoFormPage} from "../todo-form/todo-form";


/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todo',
  templateUrl: 'todo.html',
})
export class TodoPage {

  pageDetails:any = TodoDetailsPage;
  formPage: any = TodoFormPage;
  todoList:any[] = [
    { id:1, titre: "Titre1", body:"eaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaserf"},
    { id:2, titre: "Titre2", body:"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiihhhhh"}
  ];
  selectedTodo:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public srv:TodoProvider,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoPage');
    this.srv.getAllTodos().subscribe(arrayTodos=>{
      this.srv.tableauDesTodos = arrayTodos;
    })
  }

  itemSelected(todo){
    this.navCtrl.push(this.pageDetails, todo);
   // this.navCtrl.push(this.formPage, {todo:todo});

  }

  actionSwipe(event){
    this.selectedTodo = event;
    // console.log(event);
    let actionSheet = this.actionSheetCtrl.create({
    title: 'Action on todo ' + event.titre,
    //title: todo.titre,
     buttons: [
      {
      text: 'Modifier',
       role: 'modify',
      handler: () => {
        // console.log('Modify clicked');
        this.showTodoForm(this.selectedTodo);
       }
     },{
     text: 'Supprimer',
        handler: () => {
        // console.log('Destructive clicked');
        this.srv.deleteTodos(this.selectedTodo).subscribe(data=>{
            this.srv.tableauDesTodos = this.srv.tableauDesTodos.filter((todo)=>{
            return todo.id != this.selectedTodo.id
          })
        })
        }
       }
     ]
     });
    actionSheet.present();
  }

  showTodoForm(todo){
    let modal = this.modalCtrl.create(TodoFormPage,{todo:todo},{showBackdrop:true});
    modal.present();
  }
}
