import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';

import {TodoPage} from '../todo/todo';
import {ContactPage} from '../contact/contact';
import {AboutPage} from '../about/about';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pageHome:any= TodoPage;
  contact:any= ContactPage;
  about:any= AboutPage;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {

  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }

}
