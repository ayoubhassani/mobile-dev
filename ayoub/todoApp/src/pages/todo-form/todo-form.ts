import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {TodoProvider} from "../../providers/todo/todoProvider";

/**
 * Generated class for the TodoFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-todo-form',
    templateUrl: 'todo-form.html',
})
export class TodoFormPage {
    todo: any = []

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public srv: TodoProvider) {
        this.todo = navParams.get('toto');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TodoFormPage');
    }

    cancelForm() {
        this.viewCtrl.dismiss()
    }

    ValidateForm() {
        if(this.todo.id ==undefined){
            //Ajout
            this.srv.addTodo(this.todo).subscribe(addedTodo=>{
                this.srv.tableauDesTodos.push(addedTodo);
                this.viewCtrl.dismiss();
            });
        }else{
            //Modif
            this.srv.updateTodo(this.todo).subscribe(addedTodo=>{
                //this.srv.tableauDesTodos.push(addedTodo);
                this.viewCtrl.dismiss();
            });
        }
    }
}
