import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {TodoDetailsPage} from "../todo-details/todo-details";
import {TodoProvider} from "../../providers/todo/todoProvider";
import {TodoFormPage} from "../todo-form/todo-form";

@IonicPage()
@Component({
    selector: 'page-todo',
    templateUrl: 'todo.html',
})
export class TodoPage {
    PageDetails: any = TodoDetailsPage;
    formPage : any = TodoFormPage;
    todoList: any[] = [
        {id: 1, titre: "Titre1", body: "Body ertzertgzertg"},
        {id: 2, titre: "Titre2", body: "Bodyz zertzertzer"}
    ];
    selectedTodo: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public srv: TodoProvider,
                public actionSheetCtrl: ActionSheetController,
                public modalCtrl: ModalController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TodoPage');
        this.srv.getAllTodos().subscribe(arrayTodos => {
            this.srv.tableauDesTodos= arrayTodos;
        });
    }

    itemSelected(todo) {
        this.navCtrl.push(this.PageDetails, todo);
        this.navCtrl.push(this.formPage, {toto:todo});
    }

    ShoactionSheet(event) {
        this.selectedTodo = event;
        console.log(event);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Action On Todo : '+event.titre,
            buttons: [
                {
                    text: 'update',
                    handler: () => {
                        console.log('update clicked');
                        this.showTodoForm(this.selectedTodo);
                    }
                }, {
                    text: 'Delete',
                    role: 'destructive',
                    handler: () => {
                        this.srv.deleteTodo(this.selectedTodo).subscribe(data => {
                            this.srv.tableauDesTodos = this.srv.tableauDesTodos.filter((todo) => {
                                return todo.id != this.selectedTodo.id;
                            });
                        });
                        console.log('delete clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }

    showTodoForm(todo) {
        let modal = this.modalCtrl.create(TodoFormPage, {todo: todo});
        modal.present();

    }
}
