import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

/*
 Generated class for the TodoProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class TodoProvider {
    url: string = "http://192.168.1.38:8080/todo";
    tableauDesTodos=[];
    constructor(public http: Http) {
        console.log('Hello TodoProvider Provider');
    }

    getAllTodos():Observable<Todo[]> {
       return this.http.get(this.url).map(res=>res.json());
    }
    addTodo(param):Observable<Todo>{
       return this.http.put(this.url,param).map(res=>res.json());
    }
    findTodos(titre) {
        return this.http.get(this.url + '/search/' + titre).map(res => res.json());
    }
    deleteTodo(todo) {
        return this.http.delete(this.url, {body: todo}).map(res => res.json());
    }

    updateTodo(todo) {
        return this.http.post(this.url, todo).map(res => res.json());
    }




}

interface Todo{
    id:number;
    titre:string;
    body:string;
}
