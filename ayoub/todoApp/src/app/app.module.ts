import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {AboutPage} from "../pages/about/about";
import {ContactPage} from "../pages/contact/contact";
import {TodoPage} from "../pages/todo/todo";
import {TodoDetailsPage} from "../pages/todo-details/todo-details";
import {TodoProvider} from '../providers/todo/todoProvider';
import {HttpModule} from "@angular/http";
import {TodoFormPage} from "../pages/todo-form/todo-form";
import {FormsModule} from "@angular/forms";
import {Geolocation} from "@ionic-native/geolocation";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import { GoogleMaps } from '@ionic-native/google-maps';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    ContactPage,
    TodoPage,
    TodoDetailsPage,
    TodoFormPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    ContactPage,
    TodoPage,
    TodoDetailsPage,
    TodoFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    NativeGeocoder,
    TodoProvider,
    GoogleMaps
  ]
})
export class AppModule {
}
