import {Component, ViewChild} from '@angular/core';
import {MenuController, ModalController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {ContactManagerProvider} from "../providers/contact-manager/contact-manager";
import {UpdateFormPage} from "../pages/update-form/update-form";
import {ScreenOrientation} from "@ionic-native/screen-orientation";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  updateForm=UpdateFormPage;
  rootPage: any = HomePage;
  pages: Array<{ title: string, component: any }>;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private menu: MenuController,
              private InContact: ContactManagerProvider,
              public modalCtrl: ModalController,
              private screenOrientation: ScreenOrientation) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'All Contacts', component: HomePage},
      {title: 'New Contact', component: ListPage},
      {title: 'Todos', component: HomePage},
      {title: 'Todos2', component: HomePage}

    ];
    //this.menu.enable(true, 'menu1');
    // this.menu.enable(true, 'menu2');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
     // this.splashScreen.hide();
      this.screenOrientation.onChange().subscribe(
        () => {
          console.log("Orientation Changed => "+this.screenOrientation.type);
        }
      );
    });
  }
  openEdit(e){
    console.log("Opening")
      this.modalCtrl.create(this.updateForm,{contact:{test:'dfsgjkhfj'}}).present();
  }
  openPage(page) {

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
