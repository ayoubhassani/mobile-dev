import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ContactManagerProvider} from "../../providers/contact-manager/contact-manager";
import {SplashScreen} from "@ionic-native/splash-screen";
import {DeviceOrientation, DeviceOrientationCompassHeading} from "@ionic-native/device-orientation";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contactArray = [];
subscriptionWatcher;

  constructor(public navCtrl: NavController, public InContact: ContactManagerProvider,
              private splashScreen: SplashScreen,
              private deviceOrientation: DeviceOrientation) {
    this.subscriptionWatcher = this.deviceOrientation.watchHeading().subscribe(
      (data: DeviceOrientationCompassHeading) => console.log(data)
    );
  }

  ionViewDidLoad() {
    console.log("View Home Loaded Triing to get Contacts List");
    this.InContact.getContactList().then(data => {
      console.log(data);
      this.contactArray = data;
      setTimeout(() => {
        this.splashScreen.hide();
      }, 2000);
    });

  }

  getContact(event) {

  }

  contactSelected(a) {
    this.InContact.curentContact = a;
    this.InContact.transToDto(a);
  }


}
